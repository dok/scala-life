package ru.dokwork.life

class GameLoop(val frameTime: Long, val frame: () => Unit) extends Thread {

  setName("Game loop")
  setDaemon(true)

  override def run(): Unit = {
    var time = 10L
    var start = now()
    while (true) {
      if (now() - start > frameTime - time) {
        val begin = now()
        frame()
        time = now() - begin
        start = now()
      }
      Thread.sleep(1)
    }
  }

  private def now(): Long = System.currentTimeMillis()
}
