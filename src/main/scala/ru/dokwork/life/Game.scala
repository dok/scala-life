package ru.dokwork.life

import java.awt.event.{ActionEvent, ActionListener}
import java.awt.{BorderLayout, Dimension}
import javax.swing.{JButton, JFrame}

/**
  * Implementation of the game Life.
  *
  * @author vladimir
  */
object Game extends App with ActionListener {
  val (height, width) = readArgs(args)
  var pause = true
  val initialState = State(height, width)
  val field: JField = new JField(initialState)
  field.mouseCellClickListeners += (e => field.state(e.h, e.w) match {
    case true => field -(e.h, e.w)
    case false => field +(e.h, e.w)
  })

  val button = new JButton("start")
  button.setActionCommand("button pressed")
  button.addActionListener(this)

  val frame: JFrame = new JFrame("Game Life")
  frame.getContentPane.add(field, BorderLayout.CENTER)
  frame.getContentPane.add(button, BorderLayout.SOUTH)
  frame.setSize(new Dimension(350, 350))
  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  frame.setVisible(true);

  new GameLoop(1000L, () => {
    if (!pause) {
      field.state = field.state.next()
      field.repaint()
    }
  }).start()


  def actionPerformed(e: ActionEvent): Unit = {
    e.getActionCommand match {
      case "button pressed" => {
        changeGameState()
      }
    }
  }

  def changeGameState(): Unit = {
    pause match {
      case false => {
        pause = true
        button.setText("start")
      }
      case true => {
        pause = false
        button.setText("stop")
      }
    }
  }

  private def readArgs(args: Array[String]): (Int, Int) = {
    val arguments = args.partition(_.startsWith("-")).zipped.toMap
    val h = arguments.get("-h") match {
      case Some(v) => v.toInt
      case None => 15
    }
    val w = arguments.get("-w") match {
      case Some(v) => v.toInt
      case None => 15
    }
    (h, w)
  }
}
