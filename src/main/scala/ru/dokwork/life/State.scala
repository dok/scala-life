package ru.dokwork.life

import scala.collection.{BitSet, immutable}
import scala.collection.immutable.IndexedSeq

/**
  * Describes state of the game field.
  */
class State private(val height: Int, val width: Int, private val rows: Seq[BitSet]) extends Cloneable {

  def apply(h: Int, w: Int): Boolean = {
    rows(h)(w)
  }

  def +(cell: (Int, Int)): State = {
    val changedRow = rows(cell._1) | BitSet(cell._2)
    val copyRows = (rows.slice(0, cell._1) :+ changedRow) ++ rows.slice(cell._1 + 1, rows.length)
    new State(height, width, copyRows)
  }

  def -(cell: (Int, Int)): State = {
    val changedRow = rows(cell._1) &~ BitSet(cell._2)
    val copyRows = (rows.slice(0, cell._1) :+ changedRow) ++ rows.slice(cell._1 + 1, rows.length)
    new State(height, width, copyRows)
  }

  def next(): State = {
    val copyRows = 0 until height map { h: Int =>
      (BitSet.newBuilder ++= (0 until width) filter { w => isSurvived(h, w) }).result()
    }
    new State(height, width, copyRows)
  }

  private def isSurvived(h: Int, w: Int): Boolean = {
    val count = getNeighborsCount(h, w)
    (apply(h, w) && (count == 2 || count == 3)) || (!apply(h, w) && count == 3)
  }

  def getNeighborsCount(h: Int, w: Int): Int = {
    neighbors(h, w) count { t => rows(t._1)(t._2) }
  }

  private def neighbors(h: Int, w: Int): Seq[(Int, Int)] = {
    val rows = h - 1 to h + 1 withFilter (0 until height contains)
    val cols = w - 1 to w + 1 withFilter (0 until width contains)
    rows flatMap (j => cols map { i => (j, i) }) filter (_ != (h, w))
  }

  override def clone(): State = {
    val copy = rows map { row => immutable.BitSet.fromBitMask(row.toBitMask) }
    new State(height, width, copy)
  }

  override def toString(): String = {
    "\n" + (rows flatMap row2String mkString)
  }

  private def row2String(row: BitSet): String = {
    row.foldLeft(" " * width) { (blank, index) => blank.updated(index, 'x') } + "\n"
  }
}

object State {
  def apply(init: Array[Array[Boolean]]): State = {
    val height = init.length
    val width = init.map(_.length).max
    new State(height, width, toCells(init))
  }

  private def toCells(array: Array[Array[Boolean]]): Seq[BitSet] = {
    array.view map { row =>
      row.view.zipWithIndex filter { _._1 } map { _._2 }
    } map indexArray2BitSet
  }

  private def indexArray2BitSet(a: Seq[Int]): BitSet = (BitSet.newBuilder ++= a).result()

  def apply(height: Int, width: Int): State = {
    val rows: IndexedSeq[BitSet] = for (i <- 0 until height) yield BitSet()
    new State(height, width, rows)
  }
}