package ru.dokwork.life

import java.awt.Graphics
import java.awt.event.{MouseAdapter, MouseEvent}
import javax.swing.JComponent

import scala.collection.mutable

/**
  * Visualisation for the {@link ru.dokwork.life.State}
  */
class JField(var state: State) extends JComponent {

  case class CellMouseEvent(state: State, h: Int, w: Int, button: Int)

  private def h = getHeight / state.height
  private def w = getWidth / state.width

  val mouseCellHoverListeners = new mutable.ListBuffer[(CellMouseEvent) => Unit]
  val mouseCellClickListeners = new mutable.ListBuffer[(CellMouseEvent) => Unit]

  addMouseListener(new MouseAdapter {
    override def mouseMoved(e: MouseEvent): Unit = {
      val j = e.getY / h
      val i = e.getX / w
      mouseCellHoverListeners.foreach(_ (new CellMouseEvent(state, j, i, 0)))
    }
    override def mouseClicked(e: MouseEvent): Unit = {
      val j = e.getY / h
      val i = e.getX / w
      mouseCellClickListeners.foreach(_ (new CellMouseEvent(state, j, i, e.getButton)))
    }
  })

  def +(cell: (Int, Int)): Unit = {
    this.state = state + cell
    repaint()
  }

  def -(cell: (Int, Int)): Unit = {
    this.state = state - cell
    repaint()
  }

  override def paintComponent(g: Graphics): Unit = {
    paintBackground(g)
    for (j <- 0 until state.height) {
      for (i <- 0 until state.width) {
        paintCell(g, j, i)
      }
    }
  }

  private def paintBackground(g: Graphics): Unit = {
    g.setColor(getForeground)
    for (j <- 0 until state.height) {
      val y = j * h
      g.drawLine(0, y, getWidth, y)
    }
    for (i <- 0 until state.width) {
      val x = i * w
      g.drawLine(x, 0, x, getHeight)
    }
  }

  private def paintCell(g: Graphics, j: Int, i: Int): Unit = {
    if (state(j, i)) {
      g.setColor(getForeground)
    } else {
      g.setColor(getBackground)
    }
    g.fillOval(i * w, j * h, w, h);
  }
}
