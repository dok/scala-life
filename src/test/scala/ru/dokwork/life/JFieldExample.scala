package ru.dokwork.life

import java.awt.{BorderLayout, Dimension}
import javax.swing.{JFrame, JLabel}

/**
  * Example of the {@link ru.dokwork.life.JField}
  */
object JFieldExample extends App {
  val state: State = State(Array(
    Array(),
    Array(false, false, true),
    Array(false, false, false, true),
    Array(false, true, true, true, false),
    Array()
  ))

  val field: JField = new JField(state)
  val label = new JLabel("Mouse position: h = ?, w = ?")

  field.mouseCellHoverListeners += (e => label.setText(s"Mouse position: h = ${e.h}, w = ${e.w}"))
  field.mouseCellClickListeners += (e => field.state(e.h, e.w) match {
    case true => field -(e.h, e.w)
    case false => field +(e.h, e.w)
  })

  val frame: JFrame = new JFrame("Example of the JField")
  frame.getContentPane.add(field, BorderLayout.CENTER)
  frame.getContentPane.add(label, BorderLayout.SOUTH)
  frame.setSize(new Dimension(200, 200))
  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  frame.setVisible(true);
}
