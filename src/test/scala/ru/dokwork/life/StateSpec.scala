package ru.dokwork.life

import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.prop.TableDrivenPropertyChecks._
import org.scalatest.words.ShouldVerb
import org.scalatest.{FunSpec, Matchers}

/**
  * Created by vladimir
  */
class StateSpec extends FunSpec with ShouldVerb with Matchers with TableDrivenPropertyChecks {
  describe("class that describes the state of the game field") {

    describe("constructor") {
      it("should take length of the first array like height and max length of the inner arrays like width") {
        // when:
        val state = State(Array(
          Array(),
          Array(false, true, false, true),
          Array(false, true)
        ))
        // then:
        state.height should be(3)
        state.width should be(4)
      }
      it("should interpret lower length of the inner arrays like arrays with the empty right cells") {
        // when:
        val state = State(Array(
          Array(true),
          Array(false, true)
        ))
        // then:
        state(0, 0) should be(true)
        state(0, 1) should be(false)
      }
    }

    describe("method toString") {
      it("should translate instance to the string representation") {
        val state = State(Array(
          Array(true, false),
          Array(false, true)
        ))

        state.toString() should be("\nx \n x\n")
      }
    }

    describe("method clone") {
      it("should create deep copy of the original") {
        val original = State(Array(
          Array(false, true),
          Array(true, false)
        ))

        val copy: State = original.clone()
        // when:
        copy should not be theSameInstanceAs(original)
        copy.toString() should be("\n x\nx \n")
      }
    }

    describe("method getNeighborsCount") {
      val state = State(Array(
        Array(true, true, true),
        Array(true, true, true),
        Array(true, true, true)
      ))
      val data = Table(
        ("h", "w", "neighbors"),
        (1, 1, 8),
        (0, 1, 5),
        (0, 0, 3)
      )
      it("should return expected count of the neighbors") {
        forAll(data) {(h, w, count) =>
          state.getNeighborsCount(h, w) should be(count)
        }
      }
    }

    describe("method next") {
      it("should generate state: \n    \n xx \n xx \n    \n") {
        // given:
        val state = State(Array(
          Array(),
          Array(false, true, true, false),
          Array(false, true, true, false),
          Array()
        ))
        // when:
        val next = state.next()
        // then:
        next.toString() should be("\n    \n xx \n xx \n    \n")
      }
      it("should generate state: \n x \n x \n x \n") {
        // given:
        val state = State(Array(
          Array(),
          Array(true, true, true),
          Array()
        ))
        // when:
        val next = state.next()
        // then:
        next.toString() should be("\n x \n x \n x \n")
      }
    }
  }
}
